# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration


lchars = 'abcdefghijklmnopqrstuvwxyz'
digits = '0123456789'
delims = '()[],'
signs = '-+'
seps = ':,'
logicals = ('and', 'or', 'not')
param_alphabet = lchars + digits + seps
alphabet = lchars + digits + delims + signs + seps

